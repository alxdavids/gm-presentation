# spac-gm

If you want to use standard latex compilation, comment out the following lines (14-16) in spac-gm.tex:

```
\usepackage{fontspec}
\setmainfont[Mapping=tex-text]{Roboto Mono}
\let\sfdefault\rmdefault
```

I noticed that the standard latex font alters the boundaries of the page a little bit, so you may also want to reduce the size of this in line 46 of beamerthemebjeldbak.sty:

```
{\Large \textcolor{barcolor}{\textbf{\textsc{\insertframetitle}}}}
```

You can do so by replacing `\Large` with `\large`, or something similar. 
